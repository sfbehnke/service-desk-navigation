### Summary
This plugin provides basic navigational elements within the standard Jira interface.
This intentionally is targeting Service Desk Customers that have Jira User permissions.

New options are available in the Projects menu -- 
![Project nav menu](src/main/resources/images/project-nav-menu.png)

A new Requests menu is present to locate active requests --
![Requests nav menu](src/main/resources/images/requests-nav-menu.png)

A new button allows you to open the Help Center from Jira --
![Create request](src/main/resources/images/extra-create-button.png)

![Help center](src/main/resources/images/portal-in-dialog.png)