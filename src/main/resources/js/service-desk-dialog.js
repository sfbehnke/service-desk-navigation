AJS.toInit(function () {
    var HEADLESS = "headless";
    var HREF = 'href';

    var dialog = AJS.dialog2('#raise-request-dialog');
    var $dialog = AJS.$('#raise-request-dialog');

    AJS.$('#raise-request-menu a').on('click', function (e) {
        e.preventDefault();

        AJS.progressBars.update('#portal-iframe-progress', 0.1);
        AJS.progressBars.setIndeterminate('#portal-iframe-progress');

        var $this = AJS.$(this);
        var url = $this.attr(HREF) + '?' + HEADLESS + '=true';

        $dialog.find('div.aui-dialog2-content div.content-root').html(
            '<iframe src="' + url + '" id="portals-iframe" frameborder=0 scrolling="yes" width="980"/>'
        );

        var $iframe = AJS.$('#portals-iframe');
        $iframe.load(resizeIframe);

        // https://stackoverflow.com/a/16248126
        function resizeIframe() {
            $iframe.height($iframe.contents().height());
            $iframe.width($iframe.contents().width());

            onIframeLoaded();
        }

        dialog.show();
    });

    AJS.$('#raise-request-close-button').on('click', function (e) {
        e.preventDefault();
        dialog.hide();
    });

    function onIframeLoaded() {
        var i = 1;
        var interval = setInterval(increment, 500);
        function increment() {
            i = i % 360 + 1;

            if (i > 4) {
                i = 4;
                clearInterval(interval);
            }

            AJS.progressBars.update('#portal-iframe-progress', i / 4);
        }
    }
});