AJS.toInit(function () {
    var DATA_KEY = 'com.sbehnke.community.jira.service-desk-navigation:sdnav-static.requests-data';
    var data = WRM.data.claim(DATA_KEY);

    if (data === undefined || data === null) {
        return;
    }

    AJS.$('li#raise-request-menu a').css('background', data['buttonBgColor']).css('color', data['buttonTextColor']);
    AJS.$('#sdnav_requests_link').addClass('aui-dropdown2-trigger-arrowless sdnav-trigger');

    if (data['myCount'] !== undefined) {
        AJS.$('#sdnav_requests_link,#sdnav_my_requests_link_lnk')
            .append('<aui-badge class="my-requests" style="background:' + data['buttonTextColor'] + ';color:' + data['textColor'] + '">' +
                data['myCount'] +
                '</aui-badge>');
    }
});