package com.sbehnke.community.jira;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named("CommunityBaseUrlProvider")
public class BaseUrlProvider extends AbstractJiraContextProvider {
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public BaseUrlProvider(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        Map<String, Object> context = new HashMap<>();

        context.put("baseUrl", applicationProperties.getString(APKeys.JIRA_BASEURL));

        return context;
    }
}
