package com.sbehnke.community.jira;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Scanned
public abstract class AbstractLinkFactory implements WebItemProvider {
    private final static Logger log = LoggerFactory.getLogger(AbstractLinkFactory.class);

    private final static String ARTIFACT_ID = "service-desk-navigation";
    final static String WEB_ITEM_PREFIX = ARTIFACT_ID + "-";

    String BASE_URL;

    Collector<WebItem, ?, List<WebItem>> AS_LIST = Collectors.toList();

    WebFragmentBuilder newBuilder(Integer weight) {
        log.debug("Get a new WebFragmentBuilder with a weight of " + weight + ".");

        return new WebFragmentBuilder(weight);
    }

    ApplicationUser remoteUserFrom(Map<String, Object> context) {
        log.debug("Got context map: " + context.toString());

        return (ApplicationUser) context.get("user");
    }
}
