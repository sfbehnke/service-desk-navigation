package com.sbehnke.community.jira;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.servicedesk.api.portal.Portal;
import com.sbehnke.community.jira.utility.PortalManagerWrapper;
import com.sbehnke.community.jira.utility.ProjectManagerWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Named("CommunityServiceDeskNavigationProvider")
public class ProjectPortalLinkFactory extends AbstractLinkFactory {
    private final static Logger log = LoggerFactory.getLogger(ProjectPortalLinkFactory.class);

    private final static String PORTAL_URL = "/servicedesk/customer/portal/";
    private final static String STYLE_PREFIX = "nav-";
    private final static String SECTION_KEY = "browse_link/sdnav-jira-nav-portal-section";

    private final ProjectManagerWrapper projectManager;
    private final PortalManagerWrapper portalManager;
    private AtomicInteger weight = new AtomicInteger();

    @Inject
    public ProjectPortalLinkFactory(
            ProjectManagerWrapper projectManagerWrapper,
            PortalManagerWrapper portalManagerWrapper,
            @ComponentImport VelocityRequestContextFactory velocityRequestContextFactory) {
        log.debug("Construct ProjectPortalLinkFactory");

        this.projectManager = projectManagerWrapper;
        this.portalManager = portalManagerWrapper;
        this.BASE_URL = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
    }

    private String formatLabel(Portal portal) {
        log.debug("Formatting label for portal " + portal.getName() + " Web Item.");

        return portal.getName();
    }

    private WebItem transformToWebItem(Portal portal) {
        log.debug("Transforming " + portal.getName() + "(" + portal.getId() + ") to WebItem Fragment.");

        return newBuilder(weight.addAndGet(10))
                .id(WEB_ITEM_PREFIX + portal.getId())
                .styleClass(STYLE_PREFIX + "link")
                .label(formatLabel(portal))
                .title(portal.getDescription())
                .webItem(SECTION_KEY)
                .url(BASE_URL + PORTAL_URL + portal.getId())
                .build();
    }

    private Collection<Project> recentProjects(ApplicationUser applicationUser) {
        log.debug("Searching for projects using " + applicationUser.getDisplayName() + " (" + applicationUser.getUsername() + ").");

        return projectManager.getRecentProjectsForUser(applicationUser);
    }

    private Collection<Portal> findPortalsFor(ApplicationUser applicationUser) {
        log.debug("Searching for portals using " + applicationUser.getDisplayName() + " (" + applicationUser.getUsername() + ").");

        return portalManager.convertProjectsIntoPortals(recentProjects(applicationUser), applicationUser);
    }

    @Override
    public Iterable<WebItem> getItems(Map<String, Object> context) {
        log.debug("Get an Iterable of WebItems.");

        return findPortalsFor(remoteUserFrom(context))
                .stream()
                .limit(4)
                .map(this::transformToWebItem)
                .collect(AS_LIST);
    }
}
