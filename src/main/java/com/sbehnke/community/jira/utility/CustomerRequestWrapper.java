package com.sbehnke.community.jira.utility;

import com.atlassian.fugue.Either;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.commons.error.AnError;
import com.atlassian.servicedesk.api.request.CustomerRequest;
import com.atlassian.servicedesk.api.request.CustomerRequestQuery;
import com.atlassian.servicedesk.api.request.ServiceDeskCustomerRequestService;
import com.atlassian.servicedesk.api.util.paging.PagedResponse;
import com.atlassian.servicedesk.api.util.paging.SimplePagedRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;

import static com.atlassian.servicedesk.api.request.CustomerRequestQuery.REQUEST_OWNERSHIP.OWNED_REQUESTS;
import static com.atlassian.servicedesk.api.request.CustomerRequestQuery.REQUEST_STATUS.OPEN_REQUESTS;

@Named("CommunityCustomerRequestWrapper")
public class CustomerRequestWrapper {
    private final static Logger log = LoggerFactory.getLogger(CustomerRequestWrapper.class);

    @ComponentImport
    private final ServiceDeskCustomerRequestService customerRequestService;

    @Inject
    public CustomerRequestWrapper(
            ServiceDeskCustomerRequestService serviceDeskCustomerRequestService) {
        this.customerRequestService = serviceDeskCustomerRequestService;
    }

    private CustomerRequestQuery ownedQueryPaged(Integer start, Integer limit) {
        return customerRequestService.newQueryBuilder()
                .requestOwnership(OWNED_REQUESTS)
                .pagedRequest(new SimplePagedRequest(start, limit))
                .build();
    }

    private CustomerRequestQuery countOwned() {
        return customerRequestService.newQueryBuilder()
                .requestOwnership(OWNED_REQUESTS)
                .requestStatus(OPEN_REQUESTS)
                .pagedRequest(new SimplePagedRequest(0, 1000))
                .build();
    }

    private Either<AnError, PagedResponse<CustomerRequest>> ownedPagedResults(ApplicationUser applicationUser, Integer start, Integer limit) {
        return customerRequestService.getCustomerRequests(applicationUser, ownedQueryPaged(start, limit));
    }

    private Either<AnError, PagedResponse<CustomerRequest>> countOwned(ApplicationUser applicationUser) {
        return customerRequestService.getCustomerRequests(applicationUser, countOwned());
    }

    public Collection<CustomerRequest> getOwnedRequests(ApplicationUser applicationUser, Integer start, Integer limit) {
        return ownedPagedResults(applicationUser, start, limit)
                .fold(e -> Collections.emptyList(), // left
                        PagedResponse::getResults); // right
    }

    public Integer getOwnedOpenCount(ApplicationUser applicationUser) {
        return countOwned(applicationUser)
                .fold(e -> Collections.emptyList(), // left
                        PagedResponse::getResults)  // right
                .size();
    }
}
