package com.sbehnke.community.jira.utility;

import com.atlassian.fugue.Either;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.commons.error.AnError;
import com.atlassian.servicedesk.api.portal.Portal;
import com.atlassian.servicedesk.api.portal.PortalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Named("CommunityPortalManagerWrapper")
public class PortalManagerWrapper {
    private final static Logger log = LoggerFactory.getLogger(PortalManagerWrapper.class);

    private final static Collector<Portal, ?, List<Portal>> COLLECTOR =
            Collectors.toList();
    private final static Predicate<Either<AnError, Portal>> CAN_VIEW = either ->
            either != null && either.isRight();

    @ComponentImport
    private final PortalService service;

    @Inject
    public PortalManagerWrapper(PortalService portalService) {
        log.debug("Construct PortalManagerWrapper");

        this.service = portalService;
    }

    private Either<AnError, Portal> find(Project project, ApplicationUser applicationUser) {
        log.debug("Searching for a Portal for project " +
                project.getName() +
                " (" + project.getKey() + ") using user " +
                applicationUser.getDisplayName() +
                " (" + applicationUser.getUsername() + ").");

        return service.getPortalForProject(applicationUser, project);
    }

    public Portal convertProjectIntoPortal(Project project, ApplicationUser applicationUser) {
        log.debug("Searching for Portals for projects " +
                project.toString() + " using user " +
                applicationUser.getDisplayName() +
                " (" + applicationUser.getUsername() + ").");

        return find(project, applicationUser).getOrNull();
    }

    public Collection<Portal> convertProjectsIntoPortals(Collection<Project> projects, ApplicationUser applicationUser) {
        return projects
                .stream()
                .map(project -> find(project, applicationUser))
                .filter(CAN_VIEW)
                .map(Either::getOrNull)
                .collect(COLLECTOR);
    }
}
