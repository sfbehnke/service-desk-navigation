package com.sbehnke.community.jira.utility;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.atlassian.jira.bc.project.ProjectAction.VIEW_ISSUES;

@Named("CommunityProjectManagerWrapper")
public class ProjectManagerWrapper {
    private final static Logger log = LoggerFactory.getLogger(ProjectManagerWrapper.class);

    private final static String SERVICE_DESK_TYPE = "service_desk";
    private final static Predicate<Project> SERVICE_DESK = project ->
            project != null && project.getProjectTypeKey()
                    .getKey()
                    .equals(SERVICE_DESK_TYPE);
    private final static Collector<Project, ?, List<Project>> COLLECTOR = Collectors.toList();

    @ComponentImport
    private final UserProjectHistoryManager historyManager;

    @Inject
    public ProjectManagerWrapper(UserProjectHistoryManager userProjectHistoryManager) {
        log.debug("Construct ProjectManagerWrapper");

        this.historyManager = userProjectHistoryManager;
    }

    public Collection<Project> getRecentProjectsForUser(ApplicationUser applicationUser) {
        log.debug("Searching user " + applicationUser.getDisplayName() +
                " (" + applicationUser.getUsername() + ")'s history for projects.");

        return historyManager.getProjectHistoryWithPermissionChecks(VIEW_ISSUES, applicationUser)
                .stream()
                .filter(SERVICE_DESK)
                .collect(COLLECTOR);
    }
}
