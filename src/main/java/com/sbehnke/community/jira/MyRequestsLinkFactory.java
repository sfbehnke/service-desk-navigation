package com.sbehnke.community.jira;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.servicedesk.api.portal.Portal;
import com.atlassian.servicedesk.api.request.CustomerRequest;
import com.sbehnke.community.jira.utility.CustomerRequestWrapper;
import com.sbehnke.community.jira.utility.PortalManagerWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Named("CommunityMyRequestsLinkFactory")
public class MyRequestsLinkFactory extends AbstractLinkFactory {
    private final static Logger log = LoggerFactory.getLogger(MyRequestsLinkFactory.class);

    private final static String PORTAL_URL = "/servicedesk/customer/portal/";
    private final static String SECTION_KEY = "sdnav_requests_link/sdnav-jira-requests-myrequests-section";
    private final static String STYLE_PREFIX = "my-request-";
    private final static String ICON_URL = "/servicedesk/customershim/secure/viewavatar?avatarType=SD_REQTYPE&avatarId=";

    private final CustomerRequestWrapper requestService;
    private final PortalManagerWrapper portalManager;
    private AtomicInteger weight = new AtomicInteger();

    @Inject
    public MyRequestsLinkFactory(
            CustomerRequestWrapper customerRequestWrapper,
            PortalManagerWrapper portalManagerWrapper,
            @ComponentImport VelocityRequestContextFactory velocityRequestContextFactory) {
        this.requestService = customerRequestWrapper;
        this.portalManager = portalManagerWrapper;
        this.BASE_URL = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
    }

    private String formatLabel(Issue issue) {
        return issue.getKey() + " " + issue.getSummary();
    }

    private String formatDescription(Issue issue) {
        return issue.getSummary();
    }

    private WebItem transformToWebItem(CustomerRequest customerRequest, ApplicationUser applicationUser) {
        log.debug("Transforming " + customerRequest.getIssue().getKey() + "(" + customerRequest.currentStatus().status() + ") to WebItem Fragment.");

        Portal portal = portalManager.convertProjectIntoPortal(customerRequest.getIssue().getProjectObject(), applicationUser);
        Issue issue = customerRequest.getIssue();


        return newBuilder(weight.addAndGet(10))
                .id(WEB_ITEM_PREFIX + customerRequest.getIssue().getId())
                .styleClass(STYLE_PREFIX + "link")
                .label(formatLabel(issue))
                .addParam("iconUrl", BASE_URL + ICON_URL + customerRequest.getRequestTypeId())
                .title(formatDescription(issue))
                .webItem(SECTION_KEY)
                .url(BASE_URL + PORTAL_URL + portal.getId() + "/" + issue.getKey())
                .build();
    }

    @Override
    public Iterable<WebItem> getItems(Map<String, Object> context) {
        ApplicationUser remoteUser = remoteUserFrom(context);
        return requestService.getOwnedRequests(remoteUser, 0, 5)
                .stream()
                .map(r -> transformToWebItem(r, remoteUser))
                .collect(AS_LIST);
    }
}
