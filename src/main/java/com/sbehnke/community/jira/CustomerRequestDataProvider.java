package com.sbehnke.community.jira;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.sbehnke.community.jira.utility.CustomerRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.config.properties.APKeys.*;

@Named("CommunityCustomerRequestDataProvider")
public class CustomerRequestDataProvider implements WebResourceDataProvider {
    private final static Logger log = LoggerFactory.getLogger(CustomerRequestDataProvider.class);

    private final CustomerRequestWrapper requestService;

    @ComponentImport
    private final JiraAuthenticationContext authenticationContext;
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public CustomerRequestDataProvider(
            CustomerRequestWrapper customerRequestWrapper,
            JiraAuthenticationContext jiraAuthenticationContext,
            ApplicationProperties applicationProperties) {
        this.requestService = customerRequestWrapper;
        this.authenticationContext = jiraAuthenticationContext;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Jsonable get() {
        Map<String, Object> context = new HashMap<>();

        ApplicationUser remoteUser = authenticationContext.getLoggedInUser();
        if (remoteUser != null) {
            context.put("remoteUser", remoteUser.getKey());
            context.put("myCount", requestService.getOwnedOpenCount(remoteUser));
        }

        context.put("buttonBgColor", applicationProperties.getDefaultBackedString(JIRA_LF_HERO_BUTTON_BASEBGCOLOUR));
        context.put("buttonTextColor", applicationProperties.getDefaultBackedString(JIRA_LF_HERO_BUTTON_TEXTCOLOUR));
        context.put("textColor", applicationProperties.getDefaultBackedString(JIRA_LF_TOP_SEPARATOR_BGCOLOR));

        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                JSONObject obj = new JSONObject();
                try {
                    for (Map.Entry<String, Object> entry : context.entrySet()) {
                        obj.put(entry.getKey(), entry.getValue());
                    }

                    writer.write(obj.toString());
                } catch (JSONException ignored) {
                }
            }
        };
    }
}
